package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Calculadora {

    public String nome;

    public Calculadora() {
    }

    public int somar(int primeiroNumero, int segundoNumero){
        if(primeiroNumero < 0 || segundoNumero < 0){
            throw  new RuntimeException("numero nao pode ser negativo");
        }
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public double somar(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero + segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);
        return bigDecimal.doubleValue();
    }

    public int multiplicar(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

    public double multiplicar(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero * segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);
        return bigDecimal.doubleValue();
    }

    public int dividir(int primerioNumero, int segundoNumero){
        int resultado = primerioNumero / segundoNumero;
        return resultado;
    }

    public double dividir(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero / segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);
        return bigDecimal.doubleValue();
    }

}
