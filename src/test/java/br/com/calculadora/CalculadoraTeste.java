package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp() {
        calculadora = new Calculadora();
    }

    @Test
    public void testaCaminhoTristeDaSoma() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            calculadora.somar(-1, 0);
        });
    }

    @Test
    public void testaASomaDeDoisNumerosInteiros() {
        int resultado = calculadora.somar(1, 2);

        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaASomaDeDoisNumerosFlutuantes() {
        double resultado = calculadora.somar(2.3, 3.4);

        Assertions.assertEquals(5.7, resultado);

    }

    @Test
    public void testaMultiplicacaoDeDoisNumerosInteiros() {
        int resultado = calculadora.multiplicar(2, 2);
        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testaMultiplicacaoDeDoisNumerosNegativos() {
        int resultado = calculadora.multiplicar(2, -5);
        Assertions.assertEquals(-10, resultado);
    }

    @Test
    public void testaMultiplicacaoDeDoisNumerosFlutuantes() {
        double resultado = calculadora.multiplicar(2.5, 2.6);
        Assertions.assertEquals(6.5, resultado);
    }

    @Test
    public void testaDivisaoDeDoisNumerosInteiros() {
        int resultado = calculadora.dividir(10, 2);
        Assertions.assertEquals(5, resultado);
    }

    @Test
    public void testaDivisaoDeDoisNumerosNegativos() {
        int resultado = calculadora.dividir(10, -5);
        Assertions.assertEquals(-2, resultado);
    }

    @Test
    public void testaDivisaoDeDoisNumerosFlutuantes() {
        double resultado = calculadora.dividir(5.5, 2);
        Assertions.assertEquals(2.75, resultado);
    }

}
